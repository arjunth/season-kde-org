<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Season;
use App\Entity\User;
use App\Form\ProjectStudentType;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class ProjectController extends AbstractController
{

    /**
     * @Route("/project/new", name="project_new")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function new(EntityManagerInterface $em, Request $request, FlashBagInterface $flashBag): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->isMentor()) {
            $flashBag->add('error', 'Mentor can\'t create new project');
            return $this->redirectToRoute('homepage');
        }

        $project = new Project();
        $form = $this->createForm(ProjectStudentType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project->setStudent($user);
            $project->setAccepted(false);
            $project->setCompleted(false);
            $em->persist($project);
            $em->flush();

            $flashBag->add('success', 'New project saved');

            return $this->redirectToRoute('profile');
        }

        return $this->render('project/create.html.twig', [
           'message' => 'Create a new project',
           'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/project/{id}/edit", name="project_edit")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Project $project
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function edit(Project $project, EntityManagerInterface $em, Request $request, FlashBagInterface $flashBag): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() !== $project->getStudent()->getId()) {
            throw new UnauthorizedHttpException("You can only modify your projects");
        }

        $form = $this->createForm(ProjectStudentType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project->setStudent($user);
            $project->setAccepted(false);
            $project->setCompleted(false);
            $em->persist($project);
            $em->flush();

            $flashBag->add('success', 'Project edited');

            return $this->redirectToRoute('profile');
        }

        return $this->render('project/create.html.twig', [
            'message' => 'Edit a project',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/project/{id}/remove", name="project_remove")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Project $project
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function removeProject(Project $project, Request $request, EntityManagerInterface $em, FlashBagInterface $flashBag): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't remove a past project.");
        }

        if ($project->getStudent()->getId() !== $user->getId()) {
            throw new UnauthorizedHttpException("You can't only remove your own project.");
        }

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($project);
            $em->flush();
            $flashBag->add('info', 'You deleted one of your project.');
            return $this->redirectToRoute('profile');
        }

        return $this->render('project/mentor.html.twig', [
            'message' => "Do you really want to remove this project",
            'button' => "Remove",
            'state' => 'danger',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/project/mentor/{id}", name="project_mentor")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Project $project
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function addMentor(Project $project, Request $request, EntityManagerInterface $em, FlashBagInterface $flashBag): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isMentor()) {
            throw new UnauthorizedHttpException("Only mentor can mentor a project");
        }

        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't mentore a past project.");
        }

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project->addMentor($user);
            $em->persist($project);
            $em->flush();
            $flashBag->add('info', 'Now mentoring ' . $project->getTitle());
            return $this->redirectToRoute('profile');
        }

        return $this->render('project/mentor.html.twig', [
            'message' => "Do you really want to mentor this project",
            'button' => "Mentor",
            'state' => 'success',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/project/stop-mentor/{id}", name="project_stop_mentor")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Project $project
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function removeMentor(Project $project, Request $request, EntityManagerInterface $em, FlashBagInterface $flashBag): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isMentor()) {
            throw new UnauthorizedHttpException("Only mentor can mentor a project");
        }

        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't stop mentoring a past project.");
        }

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project->removeMentor($user);
            $em->persist($project);
            $em->flush();
            $flashBag->add('info', 'Now mentoring ' . $project->getTitle());
            return $this->redirectToRoute('profile');
        }

        return $this->render('project/mentor.html.twig', [
            'message' => "Do you want to stop mentoring this project",
            'button' => "Stop mentoring",
            'state' => 'warning',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/projects", name="projects")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function list(EntityManagerInterface $em): Response
    {
        /** @var Season|null $activeSeason */
        $activeSeason = $em->getRepository(Season::class)
            ->findOneBy(['active' => true]);

        if ($activeSeason === null) {
            throw new HttpException("No active season, please contact admin, if you think this is an error.");
        }

        return $this->render('project/list.html.twig', [
           'projects' => $activeSeason->getProjects(),
            'season' => $activeSeason,
        ]);
    }
}
